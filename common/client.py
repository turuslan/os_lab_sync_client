import os, io, json, argparse
import sys
import hashlib

pwd, here = os.getcwd (), os.path.split (os.path.abspath (__file__))[0]
if pwd != here :
	os.chdir (here)
sys.path.append ('./depends')

from watchdog.observers import Observer
from watchdog.events import FileSystemEventHandler
from watchdog.events import EVENT_TYPE_MOVED, EVENT_TYPE_DELETED, EVENT_TYPE_CREATED, EVENT_TYPE_MODIFIED
import requests

os.chdir (pwd)

# code from http://code.activestate.com/recipes/578453-python-single-instance-cross-platform/
try :
	import fcntl
except ImportError :
	fcntl = None

class SingleInstance :

	def __init__ (self, key = 'lock') :
		self.fh = None
		self.is_running = False
		self.OS_WIN = 'win32' in sys.platform.lower ()
		self.LOCK_PATH = os.path.join (os.path.abspath (os.path.dirname (sys.argv[0])), key)
		self.do_magic ()

	def do_magic (self) :
		if self.OS_WIN :
			try :
				if os.path.exists (self.LOCK_PATH) :
					os.unlink (self.LOCK_PATH)
				self.fh = os.open (self.LOCK_PATH, os.O_CREAT | os.O_EXCL | os.O_RDWR)
			except EnvironmentError as err :
				if err.errno == 13 :
					self.is_running = True
				else :
					raise
		else :
			try :
				self.fh = open (self.LOCK_PATH, 'w')
				fcntl.lockf (self.fh, fcntl.LOCK_EX | fcntl.LOCK_NB)
			except EnvironmentError as err :
				if self.fh is not None :
					self.is_running = True
				else :
					raise

	def clean_up (self):
		try :
			if self.fh is not None :
				if self.OS_WIN:
					os.close (self.fh)
					os.unlink (self.LOCK_PATH)
				else:
					fcntl.lockf (self.fh, fcntl.LOCK_UN)
					self.fh.close ()
					os.unlink (self.LOCK_PATH)
		except :
			pass

# parse args
parser = argparse.ArgumentParser ()
parser.add_argument ('address', help = 'server address')
parser.add_argument ('port', type = int, help = 'port number, from 1 to 65536')
parser.add_argument ('--home', help = 'path to home directory; ~ by default', default = '~')
parser.add_argument ('--timeout', type = int, help = 'sync request timeout in seconds', default = 4)
parser.add_argument ('--password', help = 'password', default = '')
args = parser.parse_args ()

cfg_path = os.path.normpath (args.home)
cfg_address = args.address
cfg_port = args.port
cfg_domain = 'http://%s:%d' % (cfg_address, cfg_port)
cfg_timeout = max (args.timeout, 0)
cfg_password = args.password

# main class
class FileWatch (FileSystemEventHandler) :

	atypes = {
		EVENT_TYPE_MOVED: 'M',
		EVENT_TYPE_DELETED: 'D',
		EVENT_TYPE_CREATED: 'C',
		EVENT_TYPE_MODIFIED: 'U',
	}

	def __init__ (self) :
		self.session = requests.Session ()
		print ('init sync')
		self.init_sync ()

	def init_sync (self) :
		global cfg_password

		tt = self.send ('/dump', {'password': cfg_password})
		if tt == b'' :
			global si
			si.clean_up ()
			sys.exit ('no connection')
		ft = self.dump (self.full_path ('.'))

		td, tf = tt
		fd, ff = ft

		for f in ff :
			if f not in tf or tf[f][1] < ff[f][1] and tf[f][0] != ff[f][0] :
				print ('sUF %s' % f)
				self.on_my_event (('U', 'F', f, None))

		for d in fd :
			if d not in td :
				print ('sCD %s' % d)
				self.on_my_event (('C', 'D', d, None))

		for f in tf :
			if f not in ff or ff[f][1] < tf[f][1] and ff[f][0] != tf[f][0] :
				print ('sGF %s' % f)
				self.on_my_event (('G', 'F', f, None))


	def on_any_event (self, event) :
		super ().on_any_event (event)

		path = self.fix_slashes (self.path_norm (event.src_path))
		atype = FileWatch.atypes[event.event_type]
		ftype = 'D' if event.is_directory else 'F'
		path_2 = self.fix_slashes (self.path_norm (event.dest_path)) if atype == 'M' else None

		self.on_my_event ((atype, ftype, path, path_2))

	def on_my_event (self, e) :
		atype, ftype, path, path_2 = e

		# logging
		print (atype, ftype, path, path_2)

		d_path = {}
		d_paths = {'path_2': path_2}
		for d in (d_path, d_paths) :
			d['path'] = path
			d['password'] = cfg_password
		if (atype == 'U' or atype == 'C') and ftype == 'F' :
			with self.prepare_file (path) as f :
				self.send ('/write', d_path, f)
		elif atype == 'D' :
			self.send ('/remove', d_path)
		elif atype == 'M' :
			if os.path.split (path)[0] == os.path.split (path_2)[0] :
				self.send ('/rename', d_paths)
		elif atype == 'C' and ftype == 'D' :
			self.send ('/mkdir', d_path)
		elif atype == 'G' and ftype == 'F' :
			self.get_file (path)

	def path_norm (self, path) :
		global cfg_path

		return path[len (cfg_path) + 1:].replace (os.sep, '/')

	def prepare_file (self, path) :
		global cfg_path

		with open (cfg_path + os.sep + path, 'rb') as f :
			d = f.read ()
		return io.BytesIO (d)

	def send (self, command, d, f = None) :
		global cfg_domain, cfg_timeout, cfg_password

		jr = b''

		if f != None :
			d_, d = d, {}
			for k in d_ :
				d[k] = json.dumps (d_[k])
		try :
			r = self.session.post (
				cfg_domain + '/sync' + command,
				files = {'__file': f} if f != None else {},
				data = d if f != None else None,
				json = d if f == None else None,
				timeout = cfg_timeout
			)
			d = b''
			for c in r :
				d += c
			jr = json.loads (d.decode ())
		except :
			print ('request failed')
			pass
		return jr

	def makedirs (self, path) :
		try :
			os.makedirs (os.path.split (os.path.normpath (path))[0])
		except :
			pass

	def full_path (self, path) :
		global cfg_path

		return os.path.expanduser (cfg_path + os.sep + path)

	def get_file (self, path) :
		try :
			r = self.session.get (
				cfg_domain + '/' + '/'.join (path.split (os.sep)),
				timeout = cfg_timeout
			)
			data = b''
			for c in r :
				data += c

			path = self.full_path (path)
			self.makedirs (path)
			with open (path, 'wb') as f :
				f.write (data)
		except :
			print ('get file failed')
			pass

	def fix_slashes (self, path) :
		return path if os.sep == '/' else path.replace ('\\', '/')

	def dump (self, path) :

		path = os.path.normpath (path)
		k = len (path) + 1

		def g (d, p, ch = False) :
			if ch :
				with open (p, 'rb') as f :
					h = hashlib.md5 (f.read ()).hexdigest ()
			else :
				h = ''
			d[self.fix_slashes (p[k:])] = [h, int (os.stat (p).st_mtime)]

		ad, af = {}, {}

		for t in os.walk (path) :
			cp, sd, sf = t
			if cp != path :
				g (ad, cp)
			for cf in sf :
				fn = cp + os.sep + cf
				with open (fn, 'rb') as f :
					g (af, fn, True)

		return [ad, af]

# ensure single instance
si = SingleInstance ('lock@' + cfg_path.replace (os.sep, '@'))
if si.is_running :
	sys.exit ('application is already running')

# set up
watch = FileWatch ()

observer = Observer ()
observer.schedule (watch, cfg_path, recursive = True)
observer.start ()

input ('started\n')

si.clean_up ()
